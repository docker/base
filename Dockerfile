FROM quay.io/rockylinux/rockylinux:8

LABEL name="LIGO Base - Enterprise Linux 8"
LABEL maintainer="Adam Mercer <adam.mercer@ligo.org>"
LABEL support="Best Effort"

# enable extra repositories
RUN curl -s https://packagecloud.io/install/repositories/github/git-lfs/script.rpm.sh | bash && \
    dnf -y install https://software.igwn.org/lscsoft/rocky/8/production/x86_64/os/Packages/i/igwn-production-config-8-9.el8.noarch.rpm && \
    dnf -y install 'dnf-command(config-manager)' && \
    dnf config-manager --set-enabled powertools && \
    dnf clean all

# enable htcondor repo
RUN dnf -y install https://research.cs.wisc.edu/htcondor/repo/24.x/htcondor-release-current.el8.noarch.rpm && \
    dnf config-manager -q --save --setopt="htcondor.skip_if_unavailable=true" &> /dev/null

# add osg repository
RUN dnf -y install https://repo.opensciencegrid.org/osg/24-main/osg-24-main-el8-release-latest.rpm && \
    dnf config-manager -q --save --setopt="osg.skip_if_unavailable=true" &> /dev/null && \
    dnf config-manager -q --save --setopt="osg.exclude=*condor*" &> /dev/null

# add pegasus repository:
RUN curl -L -o /etc/yum.repos.d/pegasus.repo https://download.pegasus.isi.edu/wms/download/rhel/8/pegasus.repo

# install extra packages
RUN dnf -y install \
      bash-completion \
      epel-release \
      igwn-backports-config \
      igwn-epel-config && \
    dnf clean all

# disable upstream epel
RUN dnf config-manager --set-disabled epel

# enable swig4.1 module
RUN dnf -y module enable swig:4.1

# install available updates
RUN dnf -y update && dnf clean all
